
let getCube = 2 ;

console.log(`The cube of ${getCube} is ${getCube ** 3}.`);

// ################################

let address = ["258","Washington Ave NW","California", 90011];

let [street, municipality , city , zipcode] = address;

console.log(`I live at ${street} ${municipality}, ${city} ${zipcode}`);

// ################################

let animal = {
    name: "Lolong",
    habitat: "saltwater",
    type: "crocodile",
    weight: 1075,
    feet: 20,
    inches: 3
};
let {name,habitat,type,weight,feet,inches} = animal;

console.log(`${name} was a ${habitat} ${type}. He weighed at ${weight} kgs with a measurement of ${feet} ft ${inches} in.`);

// ################################

let numbers=[1,2,3,4,5];

// numbers.forEach((number) =>  {
//     console.log(number);
//     return number;
// });

numbers.forEach((number) => console.log(number));

// numbers.forEach((number) => number);

// ################################

let reduceNumber = numbers.reduce((total,num) => total + num );
console.log(reduceNumber);

/* SAMPLE TUTORIAL 
    let numbers=[1,2,3,4,5];
    ******** ELABORATED PROCESS  START ********
        let testNumber = numbers.reduce(myFunc);
        function myFunc(total, num) {
            return total + num;
        }
        console.log(testNumber);
    ******** ELABORATED PROCESS  END ********
    ######## SIMPLIFIED PROCESS  END ########
        let testNumber = numbers.reduce((total, num) => total + num);
        console.log(testNumber);
    ######## SIMPLIFIED PROCESS  END ########

*/

// ################################

class Dog{
    constructor(name, age, breed){
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

let dog1 = new Dog("Frankie",5,"Miniature Dachshund");

console.log(dog1);